## Engineering Logbook Week 05
| Item | Detail |
| ------ | ------ |
| Name | Nur Asfeena Binti Roslan |
| Matric No | 197470 |
| Subsystem | Design Structure |
| Group | 2 |
| Date | 25/11/2021 |

<p>&nbsp;</p>

### **1. What is the agenda this week and what are my/our goals**

**Team : Subsystem (Design & Structure)**

On Wednesday (17/11/2021) the thruster arm team have a meeting with Fiqri at Discord. The purpose of this meeting is to show Fiqri the design that we made and make a new suggestion for the improvement. We discuss the thruster arm design. From the discussion, we already stated the material that we wanted to use for the thruster arm and we also have shown the design that Asyiqin sketch. Fiqri also gives us some suggestions to improve more for the design. Thursday (18/11/2021) Asyiqin and I redesign the design and resketching it to show it again to Fiqri. We also include the material that we wanted to use for the thruster arm. On Friday, we have the subsystems presentation. Previn presented the thruster arm design to Dr Salah with the help of other team members. Amir also working on the solid works to show the design attachment to Dr Salah. Dr Salah also gives us a goggle form which is a peer assessment for each subsystem.

**Team : Group 2**

 Below is the list of the people in charge of the checker, process monitor, recorder and coordinator for this week :

| Management Role | Name |
| ------ | ------ |
| Checker | Aienuddin |
| Process Monitor | Aliff |
| Recorder | Ivan |
| Coordinator | Nurul Hanis |

For this week, we don't have any interaction other than submitting the week 4 logbook to the checker.

### **2. What discussion did you/your team make in solving a problem**

**Team : Subsystem (Design & Structure)**

We usually will WhatsApp or meet Fiqri to ask opinions about the design that we created. This way, we can know if the design is necessary or it just does not give any impact on the thruster arm. We also discuss among the team members so that everyone has a clear understanding of the design that we have created with everyone's idea.

**Team : Group 2**

For the logbook, the checker will always remind us to submit the logbook on the date that we are supposed to send the logbook. The reminder helps some of the members because they sometimes forgot to update the logbook.


### **3. How did you/your team make those decisions (method)**

**Team : Subsystem (Design & Structure)**

The method that we take is by brainstorming. We will ask a question like is the ball joint necessary to put on the design or is the material is strong enough to withstand the wind pressure. For all of the questions that we gave, someone will answer it but if we can't find the answer among ourselves, we will ask Fiqri to get the accurate answer.


### **4. Why did you/your team make that choice (justification) when solving the problem?**

**Team : Subsystem (Design & Structure)**

It will be easier and direct by questioning on what we done. When the question we cannot answer ourselves, we will ask Fiqri and this is because Fiqri will gave us a solid answer to the question that we have asked him.


### **5. What was the impact on you/your team/the project when you make that decision?**

**Team : Subsystem (Design & Structure)**

The impact that I can see is that everyone actively participated in the discussion. This is a good way because we can see what the others thinking instead of just keeping it quiet and following the crowd. In designing, we need more ideas that's why voicing out our opinions will make a huge impact on the team works.

### **6. What is the next step?**

**Team : Subsystem (Design & Structure)**

For the next step, we have already redesigned the thruster arm attachment and will finalize it with Fiqri. Then, we will also discuss the material. In a meantime, we also have started drawing the design in solid works. By the end of the week, maybe we can start using the 3D printer.

**Team : Group 2**

For the next step, we need to submit logbook week 6 and also discuss the progression of each subsystem.







