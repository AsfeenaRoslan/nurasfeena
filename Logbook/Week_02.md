## Engineering Logbook Week 02
| Item | Detail |
| ------ | ------ |
| Name | Nur Asfeena Binti Roslan |
| Matric No | 197470 |
| Subsystem | Design Structure |
| Group | 2 |
| Date | 04/11/2021 |

<p>&nbsp;</p>

### **1. What is the agenda this week and what are my/our goals**

We discuss the project milestone for both subsystems and the group. The milestone is a guideline for the project that will be conducted for 14 weeks.

**Team : Subsystem (Design & Structure)**

My team goal is to discuss the milestone in detail for instance the design for the gondola and thruster arm, the suitable material for both gondola and thruster arm, the 3D printing, the assembling of the structure, testing and lastly the final flight testing.

**Team : Group 2**

For this team, we also have created a Gantt chart that consists of all of the representative subsystems. We plan out the work that we need to do for the 14 weeks. The function of this group is to report the movement of each subsystem so that we know what happens in each subsystem.

### **2. What discussion did you/your team make in solving a problem**

**Team : Subsystem (Design & Structure)**

Firstly, we will list out the component or structures that we need to design or redesign. Next, after knowing that we have 2 main designs that need to be done, we divide ourselves into two groups which is the first group is the gondola team and the second group is the thruster arm team.

### **3. How did you/your team make those decisions (method)**

**Team : Subsystem (Design & Structure)**

By dividing into two groups, the work process will be faster and smoother. Dividing the group will allow us to focus more on the selected structure instead of struggling to design for both structures. 

### **4. Why did you/your team make that choice (justification) when solving the problem?**

**Team : Subsystem (Design & Structure)**

The main reason we divide into two teams; the gondola team and the thruster arm team is because we don't want to waste our time just focusing on one design per week. When we divide into two groups, we can do the work simultaneously. This is very efficient because we only have 14 weeks to finish up this project. 

### **5. What was the impact on you/your team/the project when you make that decision?**

**Team : Subsystem (Design & Structure)**

Dividing the groups into two team make it easier for us to discuss and give opinion for the design without having to many or overlapped opinions from the members. It also helps us to enhace our self confident by voicing out our opinions freely.

### **6. What is the next step?**

**Team : Subsystem (Design & Structure)**

For the next step for this team, we will start giving and sketching the design for the gondola and thruster arm. We also will meet Fikri to discuss more the design that my team have made and discuss if it was appropriate and suitable for this project. Moreover, we also will start to do some research on the material that we wanted to use for both the gondola and thruster arm.




