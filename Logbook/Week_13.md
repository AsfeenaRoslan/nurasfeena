## Engineering Logbook Week 13
| Item | Detail |
| ------ | ------ |
| Name | Nur Asfeena Binti Roslan |
| Matric No | 197470 |
| Subsystem | Design Structure |
| Group | 2 |
| Date | 21/01/2022 |

<p>&nbsp;</p>

### **1. What is the agenda this week and what are my/our goals**

**Team : Subsystem (Design & Structure)**

1. Assembling the thruster arm
3. Start doing Design and Structure report

**Team : Group 2**

 Below is the list of the people in charge of the checker, process monitor, recorder and coordinator for this week :

| Management Role | Name |
| ------ | ------ |
| Checker | Zaim|
| Process Monitor | Tareena |
| Recorder | Aliff |
| Coordinator | Nurul Hanis |

We just remind the team members to update their engineering logbooks and also the subsystem report.


### **6. What is the next step?**

**Team : Subsystem (Design & Structure)**

1. Finish up the report

**Team : Group 2**

For the next step, we need to submit logbook week 14 and also send the final report to FSI team.












