## Engineering Logbook Week 04
| Item | Detail |
| ------ | ------ |
| Name | Nur Asfeena Binti Roslan |
| Matric No | 197470 |
| Subsystem | Design Structure |
| Group | 2 |
| Date | 18/11/2021 |

<p>&nbsp;</p>

### **1. What is the agenda this week and what are my/our goals**

**Team : Subsystem (Design & Structure)**

Wednesday (10/11/2021), I and Asyiqin visited the lab to see the thruster arm. We also ask help from fiqri to see the motor vibration that was stuck on the thruster arm. We found out that the vibration of the motor wasn't dreadful compared to what we had expected. On Thursday (11/11/2021) the thruster arm team, Amir, Asyiqin and Asfeena have visited the IDP Lab to look at the thruster arm. We have a discussion among us about the design for the thruster arm because we have a little bit of misunderstanding on the vibration that happened to the thruster arm. Thursday at 8:15 p.m. Asyiqin have drawn the design that Amir has suggested and then we meet at discord to discuss among the thruster team (Amir, Asyiqin, Asfeena, Arvind, Previn). The discussion ended with dividing each person a role for instance Amir will draw in SolidWorks, Asyiqin and me finished the sketching and Previn and Arvind will search for suitable material for the thruster arm. Friday (12/11/2021), Irfan already created a new file for both the gondola and thruster arm in GitLab. The purpose of the files is to put everything that we already found such as an article, sketching, photo and more that we take during the project.

**Team : Group 2**

 Below is the list of the people in charge of the checker, process monitor, recorder and coordinator for this week :

| Management Role | Name |
| ------ | ------ |
| Checker | Ivan |
| Process Monitor | Aienuddin |
| Recorder | Asfeena |
| Coordinator | Nurul Hanis |

We also shared/discuss each subsystem progress so that we know what happen on the other team.

### **2. What discussion did you/your team make in solving a problem**

**Team : Subsystem (Design & Structure)**

Firstly, we will ask each of the team ideas on how to solve the problem that we faced. Then after that, we will choose a suitable solution for the problem. It always takes us more than 2 days to think about the suitable design for the thruster arm. With all of the ideas, we manage to bring out a new improvement that can be made for the thruster arm.


### **3. How did you/your team make those decisions (method)**

**Team : Subsystem (Design & Structure)**

We discuss among us by comparing and listing the advantages and disadvantages of the design and how we can improve the design and also more importantly how we can reduce the weight of the thruster arm.


### **4. Why did you/your team make that choice (justification) when solving the problem?**

**Team : Subsystem (Design & Structure)**

Discussion thru online software is the only way we can do it right now because 2 of my teammates (Arvind and Previn) was a suspect to COVID 19. With the discussion, we also can know others ideas on how to improve the thruster arm. It also shows the participation of all the group members in this project.


### **5. What was the impact on you/your team/the project when you make that decision?**

**Team : Subsystem (Design & Structure)**

The impact that I got from the discussion is we now know where to focus on the improvement of the thruster arm. We also can see each other creativeness in how to solve the problem that we faced.

### **6. What is the next step?**

**Team : Subsystem (Design & Structure)**

For the next step, we would discuss with fiqri on the design and material for the thruster arm.

**Team : Group 2**

For the next step, we need to submit logbook week 5 and also discuss the progression of each subsystem.






