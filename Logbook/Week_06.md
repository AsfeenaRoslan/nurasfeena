## Engineering Logbook Week 06
| Item | Detail |
| ------ | ------ |
| Name | Nur Asfeena Binti Roslan |
| Matric No | 197470 |
| Subsystem | Design Structure |
| Group | 2 |
| Date | 03/12/2021 |

<p>&nbsp;</p>

### **1. What is the agenda this week and what are my/our goals**

**Team : Subsystem (Design & Structure)**
The thruster arm is divided into three parts: the thruster arm, the motor and servo, and the ball joint. The components are then drawn in engineering software such as SOLIDWORKS, CATIA V5, and Fusion 360. Amir drew the thruster arm in Solidworks, Arvind and Prevind drew the motor and servo part in Fusion 360, and Asyiqin and I drew the ball joint parts in CATIA. As you can see, we use different types of software because some of us don't have enough storage on our laptops to download new applications. However, we will use SOLIDWORKS to assemble all of the components. Next, on friday at 10 a.m, both me and Asyiqin met Fiqri to discuss on the 3D drawing that we drew and also the next step that we need to do for next week. Fiqri told us to make a list of the components needed to buy such as screw, nut or bolt that related to the thruster arm. Then, on Friday at 10 a.m., Asyiqin and I met with Fiqri to discuss about the 3D drawing we made and the next step we need to do for next week. Fiqri told us to compile a list of the parts we would need to purchase, such as screws, nuts, and bolts for the thruster arm.


**Team : Group 2**

 Below is the list of the people in charge of the checker, process monitor, recorder and coordinator for this week :

| Management Role | Name |
| ------ | ------ |
| Checker | Aliff |
| Process Monitor | Zaim |
| Recorder | IAienuddin |
| Coordinator | Nurul Hanis |

We just remind the team members to update their engineering logbooks this week. Hanis from the payload subsystem also informed us that the payload's current weight is 7kg. The members are notified using the Whatsapp application.

### **2. What discussion did you/your team make in solving a problem**

**Team : Subsystem (Design & Structure)**

We divide the drawing parts into 3 because it will be faster and efficient. All of the team members will have a responsibility so that it will be fair and square. Other than that, it will be much easier to divide the work so it will not burden any of the members if only one member draws all of the components.


**Team : Group 2**

For the logbook, the checker will always remind us to submit the logbook on the date that we are supposed to send the logbook. The reminder helps some of the members because they sometimes forgot to update the logbook. We will list names who have done their logbook so that the ones who don't submit their logbook will do their work faster.


### **3. How did you/your team make those decisions (method)**

**Team : Subsystem (Design & Structure)**

We first decided that only one person would draw the components, but after three days, we discovered that the individual was having difficulty drawing all of them. As a result, we decide to divide the work so that the person in charge of the drawing is not overburdened. We simply make a list of the components we need to draw and put our names next to the parts we want to draw in the Whatsapp group.


### **4. Why did you/your team make that choice (justification) when solving the problem?**

**Team : Subsystem (Design & Structure)**

The working process is quicker compared than drawing it alone. This is teh reason why we divide the job so that we can finish the drawing sooner


### **5. What was the impact on you/your team/the project when you make that decision?**

**Team : Subsystem (Design & Structure)**

The result is that the working process is significantly smoother and faster. When we start drawing the components, we don't feel overburdened. We can communicate more with our teammates by dividing the job. For example, we must have the same diameter or characteristics so that we can assemble the components without error after drawing.

### **6. What is the next step?**

**Team : Subsystem (Design & Structure)**

For next week, we will finalize our drawing with Fiqri and 3D print the drawing may be on Thursday. We will also start to search and buy the components that the truster's arm need.

**Team : Group 2**

For the next step, we need to submit logbook week 7 and also discuss the progression of each subsystem.








