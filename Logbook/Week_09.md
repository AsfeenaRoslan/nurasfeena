## Engineering Logbook Week 09
| Item | Detail |
| ------ | ------ |
| Name | Nur Asfeena Binti Roslan |
| Matric No | 197470 |
| Subsystem | Design Structure |
| Group | 2 |
| Date | 24/12/2021 |

<p>&nbsp;</p>

### **1. What is the agenda this week and what are my/our goals**

**Team : Subsystem (Design & Structure)**

Print out all of the thruster arm components and buy the screw and nut that was listed by the team.

**Team : Group 2**

 Below is the list of the people in charge of the checker, process monitor, recorder and coordinator for this week :

| Management Role | Name |
| ------ | ------ |
| Checker | Ivan |
| Process Monitor | Aienuddin |
| Recorder | Asfeena |
| Coordinator | Nurul Hanis |

We just remind the team members to update their engineering logbooks this week. We also started doing the resume.

### **2. What is the next step?**

**Team : Subsystem (Design & Structure)**

Start to assemble the thruster arm with a complete component that has been printed.


**Team : Group 2**

For the next step, we need to submit logbook week 10, discuss the progression of each subsystem and also update the resume.










