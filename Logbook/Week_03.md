## Engineering Logbook Week 03
| Item | Detail |
| ------ | ------ |
| Name | Nur Asfeena Binti Roslan |
| Matric No | 197470 |
| Subsystem | Design Structure |
| Group | 2 |
| Date | 11/11/2021 |

<p>&nbsp;</p>

### **1. What is the agenda this week and what are my/our goals**

**Team : Subsystem (Design & Structure)**

On Wednesday at 9 am, we have a goggle meet with Fikri. The purpose of the meeting is to discuss in detail the thruster arm and gondola design. Fikri gave us a very detailed explanation of the previous design for both mechanisms. He told us about the problem that the previous design had been through and suggest a new design for the gondola and an improvement for the thruster arm. Next, after the meeting with Fikri, on Thursday at 10 pm, the thruster arm have a meeting. The team consists of Asfeena, Asyiqin, Amir, Arvind and Previn. Due to the celebration of Deepavali, Arvind and Prevind were absent. Me, Asyiqin and Amir had discussed the idea of how to improve the thruster arm. The discussion ended with one idea that was suggested by Asfeena. We also present the Gantt chart to other subsystems in discord.

**Team : Group 2**

For this team, what we have done is we have been ask by Dr.Salah to appoint a checker, a process monitor, a recorder and a corrdinator. Below is the list of the person incharge:

| Management Role | Name |
| ------ | ------ |
| Checker | Nur Asfeena Binti Roslan |
| Process Monitor | Ivan Chua |
| Recorder | Tareena |
| Coordinator | Nurul Hanis |

We also shared/discuss each subsystem progress so that we know what happen to the other team.

### **2. What discussion did you/your team make in solving a problem**

**Team : Subsystem (Design & Structure)**

For the thruster arm team, we have discussed with each other using goggle meetings. We mostly discuss how to reduce the motor vibration on the thruster arm. Using google meet, we sketch the idea that we have to show what we thought about.

**Team : Group 2**

For this team, we volunteer to take the role that was given by Dr Salah. Every member will have a chance to take each role because we will rotate the roles every week. Hanis, the coordinator has done the rotation role in excel.

### **3. How did you/your team make those decisions (method)**

**Team : Subsystem (Design & Structure)**

By meeting with each other, we discuss the improvement for the thruster arm. We usually will make a meeting to inform any news or any new idea about the thruster arm. 

**Team : Group 2**

For the role changing, we use excel to make it clear and make it fair for everyone to have a chance on taking the roles that were given by Dr Salah. 

### **4. Why did you/your team make that choice (justification) when solving the problem?**

**Team : Subsystem (Design & Structure)**

The main reason we discuss the design of the thruster arm by google meet is that it will be must easier to communicate and show our idea compared by using WhatsApp or call. 

**Team : Group 2**
 
It will be easier for us to rotate the roles. With excel, we don't have to worry about choosing the next person for next week roles.

### **5. What was the impact on you/your team/the project when you make that decision?**

**Team : Subsystem (Design & Structure)**

The impact that I got from the discussion is we have shared an idea so that we can determine which one is the most appropriate to develop or improve the thruster arm.

### **6. What is the next step?**

**Team : Subsystem (Design & Structure)**

For the next step, we would like to go to the lab and see the gondola and thruster arm. We also would like to meet Fikri face to face and discuss our design.
**Team : Group 2**

For the next step, we need to submit logbook week 4 and also discuss the progression of each subsystem.





