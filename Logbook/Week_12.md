## Engineering Logbook Week 12
| Item | Detail |
| ------ | ------ |
| Name | Nur Asfeena Binti Roslan |
| Matric No | 197470 |
| Subsystem | Design Structure |
| Group | 2 |
| Date | 14/01/2022 |

<p>&nbsp;</p>

### **1. What is the agenda this week and what are my/our goals**

**Team : Subsystem (Design & Structure)**

1. Assembling the thruster arm
2. Buy turnbuckle
3. Start doing Design and Structure report

**Team : Group 2**

 Below is the list of the people in charge of the checker, process monitor, recorder and coordinator for this week :

| Management Role | Name |
| ------ | ------ |
| Checker | Tareena |
| Process Monitor | Asfeena|
| Recorder | Ivan |
| Coordinator | Nurul Hanis |

We just remind the team members to update their engineering logbooks this week. We also remind the members to send the resume to FSI team.


### **6. What is the next step?**

**Team : Subsystem (Design & Structure)**

1. Finish up the report

**Team : Group 2**

For the next step, we need to submit logbook week 12 and also discuss the progression of each subsystem. We also need to start doing the subsystem report.











