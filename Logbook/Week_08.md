## Engineering Logbook Week 08
| Item | Detail |
| ------ | ------ |
| Name | Nur Asfeena Binti Roslan |
| Matric No | 197470 |
| Subsystem | Design Structure |
| Group | 2 |
| Date | 17/12/2021 |

<p>&nbsp;</p>

### **1. What is the agenda this week and what are my/our goals**

**Team : Subsystem (Design & Structure)**

1. 3D print for ball joint part, motor and servo part and thruster arm part.

**Team : Group 2**

 Below is the list of the people in charge of the checker, process monitor, recorder and coordinator for this week :

| Management Role | Name |
| ------ | ------ |
| Checker | Tareena |
| Process Monitor | Asfeena|
| Recorder | Ivan |
| Coordinator | Nurul Hanis |

We just remind the team members to update their engineering logbooks this week. We also remind the members to start doing the resume.

### **2. What discussion did you/your team make in solving a problem**

**Team : Subsystem (Design & Structure)**

Following the completion of the drawing, we begin printing the ball joint component, followed by the thruster arm section, and finally the motor and servo part. This is because we need to test the components for the ball joint part to see if they function or otherwise.


**Team : Group 2**

For the logbook, the checker will always remind us to submit the logbook on the date that we are supposed to send the logbook. The reminder helps some of the members because they sometimes forgot to update the logbook. We will list names who have done their logbook so that the ones who don't submit their logbook will do their work faster.


### **3. How did you/your team make those decisions (method)**

**Team : Subsystem (Design & Structure)**

Because the ball joint is a new component in our design, printing it first allows us to adjust the components. We just make it like the previous design for the motor and servo parts and as well as the thruster arm component, so we don't have to stress much about the design.

### **4. Why did you/your team make that choice (justification) when solving the problem?**

**Team : Subsystem (Design & Structure)**

It will be simpler and provide us with more ideas for improving our design.


### **5. What was the impact on you/your team/the project when you make that decision?**

**Team : Subsystem (Design & Structure)**

We come up with a new way to improve the ball joint. (How can we make it more flexible?)

### **6. What is the next step?**

**Team : Subsystem (Design & Structure)**

Completed all the 3D print and buy screw and nuts that we needed for the thruster arm. Also, start assemble the thruster arm together one piece at a time.

**Team : Group 2**

For the next step, we need to submit logbook week 9 and also discuss the progression of each subsystem. We also need to start doing the resume.










