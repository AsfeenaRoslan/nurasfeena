## Engineering Logbook D-day
| Item | Detail |
| ------ | ------ |
| Name | Nur Asfeena Binti Roslan |
| Matric No | 197470 |
| Subsystem | Design Structure |
| Group | 2 |
| Group | 28/1/2022|

1. What is the agenda this week and what are my/our goals?
To attach the thruster arm and its supports on the airship body.


2. What decisions did you/your team make in solving a problem?
We replaced one carbon rod because the One carbon rod was not aligned with the velcro attachment due to the helium expansion. Furthermore, we also help the control subsyetm with the wiring. The wire in the thruster arm was disconnected.


3. How did you/your team make those decisions (method)?
We disconnect the problematic carbon rod and reconnect the new one. Other than that, we reconnect the control system wiring by open the thruster arm part to fix the disconnection.


4. Why did you/your team make that choice (justification) when solving the problem?
We need to make sure that everything reconnected so that the motor, servo and propeller work.


5. What was the impact on you/your team/the project when you make that decision?
The thruster arms are able to support the motor, servo and propeller operations while the airship fly. However, after a violent roll was made by the airship due to the lack in control with the wind, one of the carbon rod was snapped and broke, causing the propeller to fall onto the airship skin and torned the skin.

