## Engineering Logbook Week 11
| Item | Detail |
| ------ | ------ |
| Name | Nur Asfeena Binti Roslan |
| Matric No | 197470 |
| Subsystem | Design Structure |
| Group | 2 |
| Date | 07/01/2022 |

<p>&nbsp;</p>

### **1. What is the agenda this week and what are my/our goals**

**Team : Subsystem (Design & Structure)**

1. Print out all of the thruster arm components (motor and servo part and connector part).
2. Buy kevlar line, turnbuckle and velcro strap/tape.
2. Start to assemble the thruster arm.


**Team : Group 2**

 Below is the list of the people in charge of the checker, process monitor, recorder and coordinator for this week :

| Management Role | Name |
| ------ | ------ |
| Checker | Aienuddin |
| Process Monitor | Aliff |
| Recorder | Ivan|
| Coordinator | Nurul Hanis |

We just remind the team members to update their engineering logbooks and to update their resume.

### **2. What is the next step?**

**Team : Subsystem (Design & Structure)**

Finish the thruster arm assembly. 


**Team : Group 2**

For the next step, we need to submit logbook week 11, discuss the progression of each subsystem and possibly start the flying test.











