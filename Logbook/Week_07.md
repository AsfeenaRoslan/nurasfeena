## Engineering Logbook Week 07
| Item | Detail |
| ------ | ------ |
| Name | Nur Asfeena Binti Roslan |
| Matric No | 197470 |
| Subsystem | Design Structure |
| Group | 2 |
| Date | 10/12/2021 |

<p>&nbsp;</p>

### **1. What is the agenda this week and what are my/our goals**

**Team : Subsystem (Design & Structure)**

On Wednesday (1/12/2021), we have a meeting with Fiqri to finalize the design and also discuss the problem that we faced. The conclusion from the meeting is we decide to 3D print the ball joint and the rest of the components will follow up. Next, on Thursday (2/12/2021), I and Asyiqin go to the lab to start the 3D printing. We 3D print the ball joint and we found out that we need to resize the ball joint because the socket base does not fit the ball joint nut. We also need to resize the socket base to a bigger dimension.


**Team : Group 2**

 Below is the list of the people in charge of the checker, process monitor, recorder and coordinator for this week :

| Management Role | Name |
| ------ | ------ |
| Checker | Zaim |
| Process Monitor | Tareena |
| Recorder | Aliff |
| Coordinator | Nurul Hanis |

We just remind the team members to update their engineering logbooks this week.

### **2. What discussion did you/your team make in solving a problem**

**Team : Subsystem (Design & Structure)**

We decide to start 3D print the ball joint first because the other components was still on the design process. Also the 3D printing takes time so that's why we decide to just print any components that already get the green light from Fiqri.


**Team : Group 2**

For the logbook, the checker will always remind us to submit the logbook on the date that we are supposed to send the logbook. The reminder helps some of the members because they sometimes forgot to update the logbook. We will list names who have done their logbook so that the ones who don't submit their logbook will do their work faster.


### **3. How did you/your team make those decisions (method)**

**Team : Subsystem (Design & Structure)**

We just decide that we need to be faster because 3D printing consumes a lot of time. We also think that by 3D printing the components earlier we can improve any problem that that happen during the 3D printing.


### **4. Why did you/your team make that choice (justification) when solving the problem?**

**Team : Subsystem (Design & Structure)**

If we wait for the otehr components to finished drawing, it will take a longer time. So instead waiting, we decided that we can start 3D printing with the components that have completed.


### **5. What was the impact on you/your team/the project when you make that decision?**

**Team : Subsystem (Design & Structure)**

We get to know that we need to rezise the dimesion of both the socketbase and nut of the ball jpint. We also need to change a little bit of the socketbase shape.

### **6. What is the next step?**

**Team : Subsystem (Design & Structure)**

We will resize the ball joint and start 3D print for all of the components.

**Team : Group 2**

For the next step, we need to submit logbook week 8 and also discuss the progression of each subsystem.









