# INTRODUCTION
## Good Morning/Afternoon :smiley:
<img src="/uploads/4ecb79944ba14d3782ee8459f7b52491/IMG_E2984.JPG" width="200" height="200" />

### 
Name : Nur Asfeena Binti Roslan
###
From : Shah Alam, Selangor
###
Hobbies : 
1. Baking :doughnut:
2. Swimming :swimmer:
3. Travelling :airplane:

### Strength & Weakness
| Strength | Weakness|
| ------ | ------ |
| Organized | Lack of confidence |
| Supportive | Procrastination |


